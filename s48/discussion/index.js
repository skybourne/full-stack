let posts = []
let count = 1

// Add new post
document.querySelector("#form-add-post").addEventListener('submit', (event) => {
	// 'preventDefault' function stops the default behavior of forms when submitting them which is refreshing the page.
	event.preventDefault();

	// Pushes a new post object into the 'posts' array
	posts.push({
		id: count,
		title: document.querySelector('#txt-title').value,
		body: document.querySelector('#txt-body').value
	})

	// Iterates the count variable which will be used as the value of the ID of the post
	count++

	// Responsible for displaying each post in the HTML document
	showPosts(posts)
	alert("Post successfully created!")
})

// For showing posts in the div element
const showPosts = (posts) => {
	let post_entries = ''

	// Loops through entire 'posts' array and sets their properties (id, title, body) to their respective HTML elements
	posts.forEach((post) => {
		post_entries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>

				<button onclick="editPost('${post.id}')">Edit</button>
				<button onclick="deletePost('${post.id}')">Delete</button>
			</div>
		`
	})

	// By using 'innerHTML' we are able to convert the string format of 'post_entries' variable into HTML format
	document.querySelector('#div-post-entries').innerHTML = post_entries
}


// Edit post
// Transfers the id, title, and body to the input fields of the edit post form
const editPost = (post_id) => {
	let title = document.querySelector(`#post-title-${post_id}`).innerHTML
	let body = document.querySelector(`#post-body-${post_id}`).innerHTML

	document.querySelector("#txt-edit-id").value = post_id 
	document.querySelector("#txt-edit-title").value = title 
	document.querySelector("#txt-edit-body").value = body 
}

// Update post
document.querySelector("#form-edit-post").addEventListener('submit', (event) => {
	event.preventDefault()

	// Loops through the entire 'posts' array to find a matching post with the same ID in the form
	for(let i = 0; i < posts.length; i++){

		// If a post has been found with the same ID in the form, then update that post
		if(posts[i].id.toString() === document.querySelector("#txt-edit-id").value) {

			// Re-assigns the current title and body of the matching post to the new values from the edit form
			posts[i].title = document.querySelector("#txt-edit-title").value
			posts[i].body = document.querySelector("#txt-edit-body").value

			showPosts(posts)
			alert("Post successfully updated!")

			break 
		}

	}
})
