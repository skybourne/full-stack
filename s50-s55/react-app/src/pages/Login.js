import { useState, useEffect, useContext } from 'react';
import { Form, Button, Row, Col } from 'react-bootstrap'
import { Navigate } from 'react-router-dom'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'

export default function Login(){
	// The user, and setUser are the data that is coming from the value of the UserProvider in App.js
	// For us to be able to use these data in the Login component, we have to initialize them using 'useContext' and provide the specific context we want to use.
	const { user, setUser } = useContext(UserContext)

	const [email, setEmail] = useState('')
	const [password, setPassword] = useState('')
	const [isActive, setIsActive] = useState(false)


	function loginUser(event){
		event.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(response => response.json())
		.then(result => {
			// Checks if the response is an access token. If not, then it won't store it to the localStorage
			if(typeof result.accessToken !== 'undefined'){
				// Not only do we set the token in the localStorage, we also set the user details to the global 'user' state/context
				localStorage.setItem('token', result.accessToken)
				retrieveUserDetails(result.accessToken)

				// For clearing out the form
				setEmail('')
				setPassword('')

				// Show an alert
				Swal.fire({
					title: "Login Successful pareh",
					icon: "success",
					text: "Welcome to Zuitt!"
				})
			} 
		}).catch(error => {
			// Show an error alert
			Swal.fire({
				title: "Authentication failed my guy",
				icon: "error",
				text: "Kindly check your login credentials."
			})
		})

		
	}

	const retrieveUserDetails = (token) => {
		fetch('http://localhost:3001/users/details', {
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(response => response.json())
		.then(result => {
			setUser({
				id: result._id,
				isAdmin: result.isAdmin
			})
		})
	}

	useEffect(() => {
		if(email !== '' && password !== ''){
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [email, password])


	// We're conditionally rendering the login form so that it will only show up if the user has not yet logged in. This is possible since we are using a 'user' global state/context.
	return(
		(user.id !== null) ?
			<Navigate to="/courses"/>
		: 
		<Row>
			<Col>
				<Form onSubmit={(event) => loginUser(event)}>
					<h1>Login</h1>
			        <Form.Group controlId="userEmail">
			            <Form.Label>Email address</Form.Label>
			            <Form.Control 
			                type="email" 
			                placeholder="Enter email" 
			                value={email}
			                onChange={event => setEmail(event.target.value)}
			                required
			            />
			        </Form.Group>

			        <Form.Group controlId="password">
			            <Form.Label>Password</Form.Label>
			            <Form.Control 
			                type="password" 
			                placeholder="Password"
			                value={password}
			                onChange={event => setPassword(event.target.value)} 
			                required
			            />
			        </Form.Group>

			        { isActive ?
			        	<Button variant="primary" type="submit" id="submitBtn">
				        	Submit
				        </Button>
				      	:
				      	<Button disabled variant="primary" type="submit" id="submitBtn">
				        	Submit
				        </Button>
			        }
			    </Form>
			</Col>
		</Row>
	)
}
