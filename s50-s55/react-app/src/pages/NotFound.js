import { Fragment } from 'react'
import { Link, useNavigate } from 'react-router-dom'
import { Button } from 'react-bootstrap'

export default function NotFound(){
	// Initializing useNavigate as 'navigate' variable
	const navigate = useNavigate()

	return(
		<Fragment>
			<h1>Page Not Found</h1>
			<p> The page you are looking for cannot be found :( </p>
			<Button variant="primary" onClick={() => navigate(-1)}>Go back where you came from!</Button>
		</Fragment>
	)
}
