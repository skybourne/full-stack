import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import 'bootstrap/dist/css/bootstrap.min.css'; // Importing react-bootstrap package

// Gets the root element from the HTML file and sets it as the root component for the whole application
const root = ReactDOM.createRoot(document.getElementById('root'));

// The 'render' function renders sub-components into the root component
root.render(
  // StrictMode will throw erros in a descriptive manner and will stop your application anytime there is an error/warning whether it is big or small.
  <React.StrictMode>
    <App />
  </React.StrictMode>
);


