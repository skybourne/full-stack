import { useState, useEffect } from 'react';
import {Card, Button} from 'react-bootstrap';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

export default function CourseCard({course}){
  // You can destructure props twice to be able to access their properties directly without using dot (.) notation in the JSX code.
  const {_id, name, description, price} = course 

  // // Declaration of the 'count' state
  // // To declare a state, you must have a getter and setter.
  // // The getter is responsible for getting the value of the state which is within the useState() function
  // // The setter is responsible for setting a new value to the state.
  // // Note: You cannot change the value of the state without using the setter.
  // const [count, setCount] = useState(0);
  // const [seats, setSeats] = useState(10);
  // const [isOpen, setIsOpen] = useState(true);

  // // ACTIVITY (1 hr) - 9:25pm
  // // -Create a 'seats' state which will represent the number of available seats in the course. The value of the seats state will be 10
  // // -Put the value the seats state in the card itself
  // // -Everytime the 'enroll' button is clicked, the seats state must go down in value by 1.
  // // Stretch goal: When the value of the seats state is zero, disable the 'enroll' button

  // function enroll(){
  //  setCount(count + 1)
  //  setSeats(seats - 1)
  // }

  // // 'useEffect' has 2 arguments, an arrow function and an array. The array will hold the specific state that you want to observe changes for. If the is a change in the state inside the array, then the arrow function will run. You can leave the array empty and this will not observe any state.
  // useEffect(() => {
  //  if(seats === 0){
  //    setIsOpen(false)
  //  }
  // }, [seats])

  return(
    <Card className="my-3"> 
      <Card.Body>
        <Card.Title>{name}</Card.Title>

        <Card.Subtitle>Description:</Card.Subtitle>
        <Card.Text>{description}</Card.Text>

        <Card.Subtitle>Price:</Card.Subtitle>
        <Card.Text>Php {price}</Card.Text>

        <Link className="btn btn-primary" to={`/courses/${_id}/view`}>View Course</Link>
      </Card.Body>
    </Card>
  )
}

// You can use prop types to validate the data from props before rendering the JSX
CourseCard.propTypes = {
  course: PropTypes.shape({ //shape() function dictates the shape/structure of the props
    name: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired
  })
}
