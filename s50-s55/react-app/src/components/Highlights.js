import {Row, Col, Card} from 'react-bootstrap';

export default function Highlights(){
	return (
		<Row className="my-3">
		<Col xs={12} md={4}>
		<Card className="cardHighlight p-3">
		<Card.Body>
		<Card.Title>Home from learn</Card.Title>
		<Card.Text>
		Some quick example text to build on the card title and make up the
		bulk of the card's content.
		</Card.Text>
		</Card.Body>
		</Card>
		</Col>

		<Col xs={12} md={4}>
		<Card className="cardHighlight p-3">
		<Card.Body>
		<Card.Title>Pay now >:( , Study later ^_^</Card.Title>
		<Card.Text>
		Some quick example text to build on the card title and make up the
		bulk of the card's content.
		</Card.Text>
		</Card.Body>
		</Card>
		</Col>

		<Col xs={12} md={4}>
		<Card className="cardHighlight p-3">
		<Card.Body>
		<Card.Title>Be community of our part</Card.Title>
		<Card.Text>
		Some quick example text to build on the card title and make up the
		bulk of the card's content.
		</Card.Text>
		</Card.Body>
		</Card>
		</Col>
		</Row>
		)
}