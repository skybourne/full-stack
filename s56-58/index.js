function countLetter(letter, sentence) {
    let result = 0;

    // Check first whether the letter is a single character.
    // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
    // If letter is invalid, return undefined.

    if (letter.length == 1){
        letter = letter.toLowerCase()
        sentence = sentence.toLowerCase()
        for (let i = 0; sentence.length > i; i++){
            if (letter === sentence[i]){
                result++    
            }
        }
        console.log(result)
        return result
    } else{
        return undefined
    }
    
}


function isIsogram(text) {
    // An isogram is a word where there are no repeating letters.
    // The function should disregard text casing before doing anything else.
    // If the function finds a repeating letter, return false. Otherwise, return true.

    let textToLower = text.toLowerCase()
    let textArray = textToLower.split("")
   const uniqueLetters = new Set(textArray);
   return uniqueLetters.size === textArray.length


}


function purchase(age, price) {

    // Return undefined for people aged below 13.
    // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
    // Return the rounded off price for people aged 22 to 64.
    // The returned value should be a string.
      if (age < 13) {
        return undefined;
      } else if (age >= 13 && age <= 21) {
        const discountedPrice = (Math.round(price * 0.8 * 100) / 100).toFixed(2);
        return discountedPrice;
      } else if (age >= 22 && age <= 64) {
        const roundedPrice = price.toFixed(2);
        return roundedPrice;
      } else {
        const discountedPrice = (Math.round(price * 0.8 * 100) / 100).toFixed(2);
        return discountedPrice;
      }
    }

function findHotCategories(items) {
    // Find categories that has no more stocks.
    // The hot categories must be unique; no repeating categories.

    // The passed items array from the test are the following:

    // const items = [
    // { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' },
    // { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' },
    // { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' },
    // { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' },
    // { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }
    // ]

   const hotCategories = [];
    const uniqueCategories = new Set();

    for (let i = 0; i < items.length; i++) {
      const item = items[i];
      if (item.stocks === 0) {
        uniqueCategories.add(item.category);
      }
    }

    hotCategories.push(...uniqueCategories);

    return hotCategories;

    // The expected output after processing the items array is ['toiletries', 'gadgets'].
    // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.

}

function findFlyingVoters(candidateA, candidateB) {
    // Find voters who voted for both candidate A and candidate B.

    let flyingVoters = [];

    // The passed values from the test are the following:
    // candidateA= ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
    // candidateB= ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']

    // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
    // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.
    for (let i = 0; i < candidateA.length; i++){
        let voter = candidateA[i]
        if (candidateB.includes(voter)){
            flyingVoters.push(voter)
        }
    }
    console.log(flyingVoters)
    return flyingVoters
}

module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};