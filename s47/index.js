// The 'document' represents the whole HTML page, and the query selector targets a specific element based on its ID, class, or tag name.
console.log(document.querySelector("#txt-first-name"))

// These are the specific selectors you can use for targeting a specific target method (ID, class, tag name)
// Note: These aren't common to use anymore as compared to 'querySelector'
// document.getElementById("txt-first-name")
// document.getElementByClassName("span-full-name")
// document.getElementByTagName("input")


// [SECTION] Event Listeners
const txt_first_name = document.querySelector("#txt-first-name")
let span_full_name = document.querySelector(".span-full-name")


//The 'addEventListener' function listens for an event in a specific HTML tag. Anytime an event is triggered, it will run the function in its 2nd argument
txt_first_name.addEventListener('keyup', (event) => {
	span_full_name.innerHTML = txt_first_name.value
})

txt_first_name.addEventListener('keyup', (event) => {
	// The 'event' object represents the actual event in the HTML element (keyup) and you can use that object's 'target property to access the HTML element itself'
	console.log(event.target)
	console.log(event.target.value)
})

//  Activity: The current value of the First Name input is added as Full Name and shown in real time in  a heading.
// The current value of the Last Name input is added as Full Name and shown in real time in  a heading.

// Declare the variable for last name
let txt_last_name = document.querySelector("#txt-last-name")

txt_last_name.addEventListener('keyup' , (event) => {
	span_full_name.innerHTML = txt_first_name.value + " " + txt_last_name.value
})

// Stretch goal:
// 1. Add 2 more elements for 'age' and 'address'
// 2. Listen to the two elements and display them in a new span tag along with the first name and last name 
// 3. The value of the new span tag must be in this format:
// "Hello, I am <firstname> <lastname>, <age> years old. I am from <address>"

// Optional: Add CSS to the form and put it in the center of the page with and H1 as the heading saying 'Sir Earl is handsome'

let txt_age = document.querySelector("#txt-age")
let txt_address = document.querySelector("#txt-address")
let span_message = document.querySelector("#span-message")

txt_age.addEventListener('keyup', (event) => {
	span_message.innerHTML = `${txt_age.value} years old`
})


txt_address.addEventListener('keyup', (event) => {
	span_message.innerHTML = `Hello, I am ${txt_first_name.value} ${txt_last_name.value}, ${txt_age.value} years old. I am from ${txt_address.value}`
})

// Shorter solution:

// let updateFullNameSpan = () => {
// 	let first_name = txt_first_name.value
// 	let last_name = txt_last_name.value 

// 	span_full_name.innerHTML = `${first_name} ${last_name}`
// }

// // DRY Principle (Don't Repeat Yourself) - Instead of repeating the code for each of the event listeners, create a function that will contain that code and invoke that function within the event listeners. 

// txt_first_name.addEventListener('keyup', updateFullNameSpan)
// txt_last_name.addEventListener('keyup', updateFullNameSpan)
